/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Jonathan
 */
public class datos {
    /**private int calculo;*/
    private String nombre;
    private String email;
    private int capital;
    private int interes = 10;
    private int anios;
    
   
    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public int getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(int interes) {
        this.interes = interes;
    }

    /**
     * @return the anios
     */
    public int getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(int anios) {
        this.anios = anios;
    }
    
     /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    public int calculo (){
        
        return this.getCapital() * interes * this.getAnios() / 100;
    }
    
    public int calculo (int capital, int anios){
        this.setAnios(anios);
        this.setCapital(capital);
        
        return calculo();
    }

    public int total (){
        
        return this.getCapital() + this.calculo();
    }
    
    
}
