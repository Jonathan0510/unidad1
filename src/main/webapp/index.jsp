<%-- 
    Document   : index
    Created on : 29-03-2021, 10:45:38
    Author     : Jonathan Ahumada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <title>Unidad 1</title>
	<link rel="stylesheet" href="css/estilosform.css" type="text/css" media="all" />
	<script type="text/javascript" src="js/validaciones.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!--  -->
       <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
<body>
<CENTER>
<form onsubmit="return validacion();" action="calculos" method="POST">
<img src="img/banco.png" style="width: 15%">
<table>
<tr><td class="td0" colspan="3"><h1>Inversión a Plazo</h1></td></tr>

<tr><td class="td1"><label for="nombre">Nombre Completo</label></td><td class="td2"><input type="text" id="nombre" name="nombre"></td></tr>
<tr><td class="td1"><label for="email">Correo Electrónico</label></td><td class="td2"><input type="email" id="email" name="email"></td></tr>
<tr><td class="td1"><label for="capital">Ingrese Capital</label></td><td class="td2"><input type="number" id="capital" name="capital" ></td><td class="td3"></td></tr>
<tr><td class="td1"><label for="anios">Ingrese Anios</label></td><td class="td2"><input type="number" id="anios" name="anios"  ></td><td class="td3"></td></tr>

</table>
<br>
<br>
<button type="submit" class="btn btn-secondary" >Simular</button>
</form>
</CENTER> 
</body>
           
</html>
