<%-- 
    Document   : resultado
    Created on : 03-abr-2021, 12:14:17
    Author     : Jonathan
--%>

<%@page import="modelo.datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<% datos resultado = (datos)request.getAttribute("datos");%>

<html>
    <head>
         <title>JSP Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilosform.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
       <meta http-equiv="refresh" content="5;URL=index.jsp">
        <!--<%=resultado.calculo()%>-->
    </head>
    <center>
    <body>
        <form>
        <h1>Resultado</h1>
<table>    
    <tr><td class="td1"><label>Nombre</label></td><td class="td2"><p><%=resultado.getNombre()%></p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Email</label></td><td class="td2"><p><%=resultado.getEmail()%></p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Monto Invertido</label></td><td class="td2"><p><%=resultado.getCapital()%></p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Anios</label></td><td class="td2"><p><%=resultado.getAnios()%></p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Interes</label></td><td class="td2"><p><%=resultado.getInteres()%> %</p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Monto Ganado</label></td><td class="td2"><p><%=resultado.calculo()%></p><td class="td3"></td></tr>
    <tr><td class="td1"><label>Monto Total</label></td><td class="td2"><p><%=resultado.total()%></p><td class="td3"></td></tr>
    
</table>
    </form>
    </body>
    </center>
</html>
